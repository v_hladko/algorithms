#include <iostream>

template <typename T>
class Node {
    public:
        T value;
        Node* next;

        Node(T value) {
            this->value = value;
        }
};

template <typename Y>
class Queue {
    int length = 0;
    Node<Y>* head = nullptr;
    Node<Y>* tail = nullptr;

    public:
        void enqueue(Y value) {
            Node<Y>* node = new Node<Y>(value);

            length++;
            if(length == 1) {
                head = tail = node;
                return;
            }

            tail->next = node;
            tail = node;
            return;
        }

        Y dequeue() {
            if(length == 0) {
                std::cerr << "Queue is empty";
                return Y();
            }

            length--;
            Node<Y>* temp = head;
            Y value = temp->value;
            head = temp->next;
            
            delete temp;

            return value;
        }
};

int main() {
    Queue<int> queue = Queue<int>();

    queue.enqueue(3);
    queue.enqueue(6);

    int x = queue.dequeue();

    std::cout << x << "\n";

    int y = queue.dequeue();

    std::cout << y << "\n";

     queue.dequeue();

    return 0;
}

